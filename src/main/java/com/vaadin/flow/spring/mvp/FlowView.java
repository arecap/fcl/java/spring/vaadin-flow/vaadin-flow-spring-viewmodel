package com.vaadin.flow.spring.mvp;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.EventObject;

public interface FlowView<P extends FlowPresenter> extends BeforeEnterObserver,BeforeLeaveObserver, Serializable {

	default Logger getLogger() {
		return LoggerFactory.getLogger(getClass());
	}

	@PostConstruct
	default void init() {
		getLogger().info("init");
		getPresenter().setView(this);
		buildView();
	}

	void buildView();

	default void onAttach(AttachEvent attachEvent) {
		getLogger().info("onAttach");
		getPresenter().prepareModelAndView(attachEvent);
	}

	@Override
	default void beforeEnter(BeforeEnterEvent event) {
		getLogger().info("beforeEnter");
		UI.getCurrent().getSession().getCurrent().setErrorHandler(errorEvent -> {
			getLogger().error(errorEvent.getThrowable().getMessage(), errorEvent.getThrowable());
			Notification.show("Eroare: "+errorEvent.getThrowable().getMessage(), 10000,
					Notification.Position.BOTTOM_END);
		});
	}

	@Override
	default void beforeLeave(BeforeLeaveEvent event) {
		getLogger().info("beforeLeave");
		getPresenter().beforeLeavingView(event);
	}
	
	P getPresenter();
		
	default void beforePrepareView(EventObject event){
		getLogger().info("beforePrepareView");
	}
	
	default void prepareView() {
		getLogger().info("prepareView");
	}
	
	default void afterPrepareView() {
		getLogger().info("afterPrepareView");
	}


}
